// Copyright 2015 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build ignore

package main

import (
	"flag"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/websocket"
)

var addr01 = flag.String("addr-8080", "localhost:8080", "http service address")
var addr02 = flag.String("addr-8081", "localhost:8081", "http service address")

var result = make(chan []byte)

func receiver() {
	flag.Parse()
	log.SetFlags(0)

	interrupt := make(chan os.Signal, 1)

	signal.Notify(interrupt, os.Interrupt)

	//init url

	u01 := url.URL{Scheme: "ws", Host: *addr01, Path: "/echo"}
	log.Printf("connecting to %s", u01.String())

	c01, _, err := websocket.DefaultDialer.Dial(u01.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c01.Close()

	u02 := url.URL{Scheme: "ws", Host: *addr02, Path: "/echo"}
	log.Printf("connecting to %s", u02.String())

	c02, _, err := websocket.DefaultDialer.Dial(u02.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c02.Close()

	//end url
	done := make(chan struct{})

	go func() {
		defer close(done)
		for {

			_, message, err := c01.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			result <- message
		}
	}()

	go func() {
		defer close(done)
		for {
			_, message, err := c02.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			result <- message
		}
	}()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		//log.Printf("recv0: %s", <-result)
		select {
		case <-done:
			return
		case t := <-ticker.C:
			err := c01.WriteMessage(websocket.TextMessage, []byte(t.String()))
			if err != nil {
				log.Println("write:", err)
				return
			}
			err = c02.WriteMessage(websocket.TextMessage, []byte(t.String()))
			if err != nil {
				log.Println("write:", err)
				return
			}
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c01.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			err = c02.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}
