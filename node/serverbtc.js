const WebSocket = require("ws");

const wss = new WebSocket.Server({
  port: 8081,
});

wss.on("connection", (ws) => {
  setInterval(function () {
    const btcPrice = "BTC " + Math.floor(Math.random() * 20000).toString();
    ws.send(btcPrice);
    console.log("Feed BTC " + btcPrice);
  }, 1000);

  ws.on("message", (data) => {
    let message;

    try {
      message = JSON.parse(data);
    } catch (e) {
      //sendError(ws, "Wrong format");

      return;
    }

    if (message.type === "NEW_MESSAGE") {
      wss.clients.forEach((client) => {
        if (client !== ws && client.readyState === WebSocket.OPEN) {
          client.send(data);
        }
      });
    }
  });
});

const sendError = (ws, message) => {
  const messageObject = {
    type: "ERROR",
    payload: message,
  };

  ws.send(JSON.stringify(messageObject));
};
