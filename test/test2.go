package main

import (
	"fmt"
	"time"
)

func main() {

	num := 45
	jobs := make(chan int, num)
	results := make(chan int, num)

	go worker(jobs, results)
	go worker(jobs, results)
	go worker(jobs, results)
	go worker(jobs, results)

	for i := 0; i < num; i++ {
		jobs <- i
	}
	close(jobs)
	start := time.Now()
	for j := 0; j < num; j++ {
		fmt.Println(<-results)
	}
	elapsed := time.Since(start)
	fmt.Println(elapsed)
}

func worker(jobs <-chan int, results chan<- int) {
	for n := range jobs {
		results <- fib(n)
	}
}

func fib(n int) int {
	if n <= 1 {
		return n
	}

	return fib(n-1) + fib(n-2)
}
