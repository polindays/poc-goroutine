package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

func printA(msgs int, send chan<- string, no string) {
	start := time.Now()
	count := 0
	for i := 0; i < msgs; i++ {
		count++
		time.Sleep(time.Second * 1)
	}
	elapsed := time.Since(start)
	send <- no + " " + fmt.Sprint(elapsed) + " " + fmt.Sprint(count)
}
func main() {

	rand.Seed(time.Now().UnixNano())

	nCPU := runtime.NumCPU()
	runtime.GOMAXPROCS(nCPU)
	fmt.Println("Number of CPUs: ", nCPU)

	num := 24
	threads := 12

	timeElapsed := make(chan string, threads)

	start := time.Now()

	for j := 0; j < threads; j++ {
		go printA(num/threads, timeElapsed, fmt.Sprint(j))
	}

	for i := 0; i < threads; i++ {
		fmt.Println(<-timeElapsed)
	}
	elapsed := time.Since(start)
	fmt.Println(elapsed)
}
