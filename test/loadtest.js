import ws from "k6/ws";
import { check, stages } from "k6";

export let options = {
  stages: [
    { duration: "1s", target: 5000 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: "1m", target: 5000 }, // stay at 100 users for 10 minutes
  ],
};

export default function () {
  const url = "ws://localhost:8080/ws";
  const params = { tags: { my_tag: "hello" } };

  const res = ws.connect(url, params, function (socket) {
    socket.on("open", () => console.log("connected"));
    socket.on("message", (data) => console.log("Message received: ", data));
    socket.on("close", () => console.log("disconnected"));
  });

  check(res, { "status is 101": (r) => r && r.status === 101 });
}
// k6 run loadtest.js --out json=result.json

/*
    checks................: 100.00% ✓ 4260   ✗ 0     
    data_received.........: 5.7 MB  27 kB/s
    data_sent.............: 1.6 MB  7.3 kB/s
    iteration_duration....: avg=2m19s  min=1m0s     med=2m20s    max=3m30s   p(90)=3m13s   p(95)=3m21s  
    iterations............: 4260    19.968378/s
    vus...................: 382     min=382  max=5000
    vus_max...............: 5000    min=5000 max=5000
    ws_connecting.........: avg=5.36ms min=159.43µs med=847.91µs max=87.51ms p(90)=18.04ms p(95)=26.85ms
    ws_msgs_received......: 390538  1830.612767/s
    ws_session_duration...: avg=2m19s  min=1m0s     med=2m20s    max=3m30s   p(90)=3m13s   p(95)=3m21s  
    ws_sessions...........: 8187    38.375847/s
*/
