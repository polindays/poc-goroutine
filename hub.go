// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"
	"net/url"

	"github.com/gorilla/websocket"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

var addr01 = flag.String("addr-8081", "localhost:8081", "http service address")
var result = make(chan []byte)

func newHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) run() {

	u01 := url.URL{Scheme: "ws", Host: *addr01, Path: "/echo"}
	log.Printf("connecting to %s", u01.String())

	c01, _, err := websocket.DefaultDialer.Dial(u01.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c01.Close()

	go func() {
		for {

			_, message, err := c01.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			result <- message
		}
	}()

	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-result: //h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
