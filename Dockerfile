FROM golang:alpine3.13 as builder
WORKDIR src
RUN apk add --no-cache git
RUN git clone https://gitlab.com/polindays/poc-goroutine.git .
RUN go mod download
RUN go mod verify
RUN go build -o pocGoRoutine

FROM golang:alpine3.13 as runner
WORKDIR bin
COPY --from=builder go/src/pocGoRoutine .
EXPOSE 8080
CMD ["./pocGoRoutine"]
